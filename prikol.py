'''strings = [
(
"  ____      ____ \n"+  
" /        /\n"+
"/        /\n"+
"|  ___   |  ___\n"+
"| |_  |  | |_  |\n"+
"\     /  \     /\n"+
" \___/    \___/\n"
),
(
"  ____     ___\n"+   
" /        /\n"+
"/        /\n"+
"|  ___   |  __\n"+
"| |_  |  | |_  \n"+
"\     /  \     \n"+
" \___/    \___\n"
),(
"  ____     __\n"+   
" /        /\n"+
"/        /\n"+
"|  ___   |  _\n"+
"| |_  |  | |_  \n"+
"\     /  \     \n"+
" \___/    \__\n"
),
("  ____     _   \n"+
" /        /\n"+
"/        /\n"+
"|  ___   |  \n"+
"| |_  |  |   \n"+
"\     /  \     \n"+
" \___/    \_\n"
),
("  ____ \n"+
" /        /\n"+
"/        /\n"+
"|  ___   |  \n"+
"| |_  |  |   \n"+
"\     /  \     \n"+
" \___/    \\ \n"
),
("  ____       \n"+
" /        \n"+
"/        /\n"+
"|  ___   |  \n"+
"| |_  |  |   \n"+
"\     /  \     \n"+
" \___/    \n"
),
("  ____       \n"+
" /        \n"+
"/        \n"+
"|  ___     \n"+
"| |_  |     \n"+
"\     /       \n"+
" \___/    \n"
)]'''
strings=[[
"            __   __   __  _____   __     __  __      __ ",
"           |  | |__| |  |/ __  | |  |   |  | \ \    / / ",
"           |  |  __  |    /  | | |  |   |  |  \ \__/ /  ",
"           |  | |  | |   /   | | |  |   |  |   \    /   ",
"           |  | |  | |  |    | | |  |___|  |   / __ \\  ",
"           |  | |  | |  |    | | \         /  / /  \ \\ ",
"           |__| |__| |__|    |_|  \_______/  /_/    \_\\",
"                                             ",
"            _________   ____    ______                  ",
"           |___   ___| / __ \  |   __ \\                ",
"               | |    / /  \ \ |  |__| |                ",
"               | |    | |  | | |   ___/                 ",
"               | |    | |  | | |  |                     ",
"               | |    \ \__/ / |  |                     ",
"               |_|     \____/  |__|                     ",
],
[
"            |__|                                                 ",
"  ____    _    __        ___    __    __ ______     __  __   __  ",
" / __ \  | |  /  |      / _ \   \ \  / / |  __ \   /  | | | / /  ",
"/ /  \ \ | | /   |      || ||    \ \/ /  | |__| | / /|| | |/ /   ",
"| |  | | | |/    |      || ||     \  /   |  ___/ / /_|| |    |   ",
"| |  | | |    /| |    __||_||__   / /    | |    / ___ | | |\ \\  ",
"\ \__/ / |   / | |    |  ___  |  / /     | |   / /   || | | \ \\ ",
" \____/  |__/  |_|    |_/   \_| /_/      |_|  /_/    || |_|  \_\\",

],[
"                __   _________     _____    _     _   __   __",
"   ________    |__| |___   ___|   |  __ \  | |   | | |  | |  |",
"  /____  | \    __      | |       | |__| | | |   | | |  | |  |",
" |     | | |   |  |     | |       |  ___/  | |   | | |  | |  |",
" \_____| | |   |  |     | |       | |      | |___| | |  | |  |",
"  |      |_/   |  |     | |       | |      \       / |  | |  |",
"  /__/|__|     |__|     |_|       |_|       \_____/  |__| |__|",
],
[
"   ______      __   _________     _____    _     _   _____   _     _ ",
"  /____  \_   |__| |___   ___|   |  __ \  | |   | | /  ___| | |   | |",
" |     | | \   __      | |       | |__| | | |   | | | |___  | |___| |",
" \_____| | |  |  |     | |       |  ___/  | |   | | \___  \ |  ___  |",
"  |      | |  |  |     | |       | |      | |___| |     | | | |   | |",
"  |      |_/  |  |     | |       | |      \       /  ___| | | |   | |",
"  /__/|__|    |__|     |_|       |_|       \_____/  |____/  |_|   |_|" ,
]]

import os
import time
from termcolor import colored
import pyautogui
from PIL import Image

if bool(input("frame")):        
    os.system("clear")
    for a in strings[2]:
        print(colored(a, "green"))
    time.sleep(0.5)
    myScreenshot = pyautogui.screenshot()
    myScreenshot.save(r'/home/timofey/inages/t.png') 

    os.system("clear")
    for a in strings[3]:
        print(colored(a, "red"))
    time.sleep(0.3)
    myScreenshot = pyautogui.screenshot()
    myScreenshot.save(r'/home/timofey/inages/r.png')
    Image.open(r'/home/timofey/inages/t.png').save(r"/home/timofey/inages/final2.gif", save_all=True, 
    append_images=[Image.open(r'/home/timofey/inages/t.png'), Image.open(r'/home/timofey/inages/r.png')], duration=250, optimaze=False, loop=0) 

else:
    frame = int(input("number_frame?"))
    print(len(strings[frame][0]))
    time.sleep(1)

    data_for_gif = []
    
    for i in range(len(strings[frame][0])):
        os.system("clear")
        for a in strings[frame]:
            print(colored(a[:i], 'green'))
        time.sleep(0.1)
        myScreenshot = pyautogui.screenshot()
        myScreenshot.save(r'/home/timofey/inages/1.'+ str(i)+r".png")        
        data_for_gif.append(Image.open(r'/home/timofey/inages/1.'+ str(i)+r".png"))

    for i in reversed(range(len(strings[frame][0]))):
        os.system("clear")
        for a in strings[frame]:
            print(colored(a[:i], 'red'))
        time.sleep(0.1)
        myScreenshot = pyautogui.screenshot()
        myScreenshot.save(r'/home/timofey/inages/2.'+ str(i)+r".png")
        data_for_gif.append(Image.open(r'/home/timofey/inages/2.'+ str(i)+r".png"))

    data_for_gif[0].save(r"/home/timofey/inages/final.gif",save_all=True, append_images=data_for_gif, duration=100, optimaze=False, loop=0)














































